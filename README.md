# temperantia icons

**Appx. reading time: 3 minutes**  

The temperantia icon theme is primarily meant to be used together with the temperantia GTK (and GNOME Shell) theme. As it's been developed for this purpose, it hasn't really been tested using other GTK themes. So, although the temperantia icons *should* work fine with other GTK themes, temperantia is still the recommended one.  

A few other things to note when using the temperantia icon theme:  

* Comes in **three color variants**: rose (pink), sea (blue-green), and overcast (gray) - see previews below  

* Meant to **work well primarily with GNOME**, and isn't explicitly compatible with e.g. KDE Plasma  

* Contains **very few application icons**; most of these are instead inherited from the Adwaita icon theme  

* Although fully usable, **some solutions aren't ideal**, especially with regard to **different size variants** of the regular icons (one of many TODOs)  

* For ethical and ideological reasons, some **standardized symbols** in icon themes, like musical notes and wrenches, are consciously avoided and instead various **alternatives** are used - read more further down  

# Previews

![Screenshot of all color variants of the temperantia icon theme](images/temperantia-icons.png)  

# Installation

The temperantia icon theme can be installed for a local user only, or for system wide use. Both methods are described below. To attain the most visual consistency, though, system wide installation is recommended.  

## Local installation

**1.** To install for a local user only, **open a terminal** inside the directory `temperantia-icons`, in which all of the color variants of the temperantia icon theme are located.  

**2.** Then run the below command to, firstly, create the hidden directory `.icons` in your home folder, and then copy *all* of the color variants of the temperantia icon theme to this directory (if the directory already exists, the icon theme folders will just be copied over to it). To copy just *one* of the color variants, replace the **asterisk** below with the color of your choice.  

```
mkdir ~/.icons; cp -r temperantia-*/ ~/.icons  
```

**3.** After running the command, you should be able to choose the temperantia icon theme in e.g. GNOME Tweaks.  

## System wide installation

**1.** To install for system wide use, **open a terminal** inside the directory `temperantia-icons`, in which all of the color variants of the temperantia icon theme are located.  

**2.** Then run the below command to copy *all* the color variants to the root `icons` directory. To copy just *one* of the color variants, replace the **asterisk** below with the color of your choice.  

```
sudo cp -r temperantia-*/ /usr/share/icons  
```

**3.** After running the command, you should be able to choose the temperantia icon theme in e.g. GNOME Tweaks.

# Alternatives to standardized symbols

For primarily ethical and ideological reasons, a conscious effort has been made *not* to use some common, standardized symbols in the temperantia icon theme. These include, for example, musical notes, wrenches, and shields.  

First, using a **musical note** to generically denote music/audio is problematic for lots of reasons (too many to mention here). But one of the major issues, at least, is the fact that this particular kind of musical notation is closely connected to the European so-called "classical" music tradition. As such, the usage of these musical notes in something like an icon theme is in general Eurocentric. In particular, it further reproduces the idea that European "classical" music represents the ideal, true nature of music.  

Second, **wrenches** and other similar tools are very much associated with, and part of, many masculine practices, interests, and spaces. Arguably, the continued use of such kinds of icons is therefore one of the ways in which desktop environments are being gendered - and by extension e.g. software development, too.  

Third, **shields**, along with e.g. knives, are quite obviously related to violence. Ideally, any reference to violence, however subtle, should be avoided at all costs in an icon theme - for obvious reasons. One might also add that the world of violence and war has a long history of being closely tied to masculinity, which means the argument in the above paragraph applies here, too.  

Now, replacing symbols such as the ones discussed above, obviously isn't an easy task. And it's certainly the case that this is a *work-in-progress* with regards to the development of the temperantia icon theme. So far, though, the idea has been to replace *musical notes* with a few different symbols, depending on the kind of icon. Generally, an image of a *CD* is used to symbolize anything to do with music, and a *speaker* to symbolize audio more broadly.  

When it comes to the other kinds of problematic icons/symbols, the approach has been quite different. The main idea here has been to try to create *shapes* which are reminiscent of the ones that are to be replaced, but slightly change them, enough to become something else. This way, these icons can still be somewhat recognizable, though at the same time be different enough to have their own visual identity, so to speak.  

# Acknowledgements

* Although mostly unconsciously, the battery icons are inspired by the battery icons used in [Ubuntu touch](https://ubuntu-touch.io/ "https://ubuntu-touch.io/").  
